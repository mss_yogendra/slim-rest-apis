<?php
error_reporting(-1);
ini_set('display_errors', 'On');
require 'config/DB.php';
require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

require 'config/connect/sql.php';
$app = new \Slim\Slim();
require_once('Slim/Middleware/TokenAuth.php');

$app->get('/users','User:getUsers');
$app->add(new \TokenAuth());
$app->post('/updates','User:postUserUpdate');
$app->post('/add','User:postAddusers');
$app->delete('/updates/delete/:id','User:deleteUpdate');
$app->get('/users/search/:query','User:getUserSearch');
$app->post('/login','User:login' );
$app->post('/authenticate','User:authenticate');

$app->post('/abc','User:postAbc');
$app->run();
class User {
// GET http://www.yourwebsite.com/api/users
public function getUsers() {
	$sql =  new sql();
	$sql = $sql->select();
try {
$db = getDB();
$stmt = $db->query($sql);
$users = $stmt->fetchAll(PDO::FETCH_OBJ);
$db = null;
// $_SESSION['current_user'] = 'sdss';
// session_unset();
// session_destroy();
// echo $_SESSION['current_user'];
echo '{"users": ' . json_encode($users) . '}';
} catch(PDOException $e) {
//error_log($e->getMessage(), 3, '/var/tmp/phperror.log'); //Write error log
echo '{"error":{"text":'. $e->getMessage() .'}}';
}
}


public function postAddusers() {
	 $app = \Slim\Slim::getInstance();
   $request = $app->request();
	$first = $request->params('firstname');
	$last = $request->params('lastname');
	$address = $request->params('address');
	$phone = $request->params('phone');
$sql  = new sql();
$sql = $sql->insert();
try {
$db = getDB();
$stmt = $db->query($sql);
$db = null;
} catch(PDOException $e) {
//error_log($e->getMessage(), 3, '/var/tmp/phperror.log'); //Write error log
echo '{"error":{"text":'. $e->getMessage() .'}}';
}
}

public function postAbc(){
	   $app = \Slim\Slim::getInstance();
   $request = $app->request();
	$id = $request->post('id');
echo 'ID='.$id;
}

// DELETE http://www.yourwebsite.com/api/updates/delete/10
public function deleteUpdate($id)
{
$sql  = new sql();
$sql = $sql->delete($id);
try {
$db = getDB();
$stmt = $db->prepare($sql);
$stmt->bindParam("id", $id);
$stmt->execute();
$db = null;
echo true;
} catch(PDOException $e) {
echo '{"error":{"text":'. $e->getMessage() .'}}';
}
}

public function postUserUpdate(){
	$app = \Slim\Slim::getInstance();
   $request = $app->request();
	$first = $request->params('firstname');
	$last = $request->params('lastname');
	$address = $request->params('address');
	$phone = $request->params('phone');
	$id = $request->params('id');
$sql = new sql();
$sql =  $sql->update($id);
try {
$db = getDB();
$stmt = $db->prepare($sql);
$stmt->bindParam("id", $id);
$stmt->execute();
$db = null;
echo true;
} catch(PDOException $e) {
echo '{"error":{"text":'. $e->getMessage() .'}}';
}
}

// GET http://www.yourwebsite.com/api/users/search/sri
public function getUserSearch($query) {
//.....
//....
}
}